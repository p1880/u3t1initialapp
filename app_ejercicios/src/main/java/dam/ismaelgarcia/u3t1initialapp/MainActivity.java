package dam.ismaelgarcia.u3t1initialapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private int count;
    private static final String DEBUG_TAG = "LOG-";

    private TextView tvDisplay;
    private Button buttonIncrease, buttonDecrease, buttonReset, buttonIncrease2, buttonDecrease2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI(){
        tvDisplay = findViewById(R.id.tvDisplay);
        buttonIncrease = findViewById(R.id.buttonIncrease);
        buttonDecrease = findViewById(R.id.buttonDecrease);
        buttonReset = findViewById(R.id.buttonReset);
        buttonIncrease2 = findViewById(R.id.buttonIncrease2);
        buttonDecrease2 = findViewById(R.id.buttonDecrease2);

        buttonIncrease.setOnClickListener(this);
        buttonDecrease.setOnClickListener(this);
        buttonIncrease2.setOnClickListener(this);
        buttonDecrease2.setOnClickListener(this);
        buttonReset.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonIncrease: count += 1;break;
            case R.id.buttonDecrease: count -= 1;break;
            case R.id.buttonIncrease2: count += 2;break;
            case R.id.buttonDecrease2: count -= 2;break;
            case R.id.buttonReset:count = 0; break;
        }

        tvDisplay.setText(getString(R.string.number_of_elements) + ": " + count);
    }


    //se llama cuando se destruye la aplicacion, como cuando giras la pantalla
    protected void onRestoreInstanceState(Bundle savedInstanceState){
        super.onRestoreInstanceState(savedInstanceState);
        count =savedInstanceState.getInt("count");
        Log.i(DEBUG_TAG+ getLocalClassName(), " onRestore ");
    }
    //sirve para crear un save de la aplicacion.
    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putInt("cont", count);
        Log.i(DEBUG_TAG+ getLocalClassName(), " onSave ");
    }



}